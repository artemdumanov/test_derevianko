# urls.py
from unicodedata import name

from django.urls import path
from house.views import HouseList, HouseResidentsList

urlpatterns = [
    path('house/all/', HouseList.as_view(), name='houses'),
    path('house/<int:pk>/', HouseResidentsList.as_view(), name='house'),
]