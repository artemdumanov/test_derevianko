from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Model, CharField, IntegerField, PositiveIntegerField


class House(Model):
    address = CharField(max_length=255, blank=False, null=False)
    residence_count = PositiveIntegerField(blank=False, null=False)
    year_of_construction = IntegerField(blank=False,
                                        null=False,
                                        validators=[MinValueValidator(1900),
                                                    MaxValueValidator(2020)]
                                        )
