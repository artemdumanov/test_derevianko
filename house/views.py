from django.views.generic import ListView, DetailView

from house.models import House


class HouseList(ListView):
    model = House
    template_name = 'house_list.html'

    def get_queryset(self):
        order_by = self.request.GET.get('order_by')
        ordering = self.request.GET.get('ordering')

        prefix = '-' if ordering == 'descending' else ''

        qs = super(HouseList, self).get_queryset()

        try:
            if order_by:
                qs = qs.order_by(prefix + order_by)
        except:
            raise
        return qs


class HouseResidentsList(DetailView):
    model = House
    template_name = 'house_resident_list.html'
