from django.contrib import admin

from house.models import House


class HouseAdmin(admin.ModelAdmin):
    list_display = ('address', 'residence_count', 'year_of_construction')


admin.site.register(House, HouseAdmin)
