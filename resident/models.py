from django.db import models
from django.db.models import Model, CharField, DateField, IntegerField, PositiveIntegerField, ForeignKey


class Resident(Model):
    full_name = CharField(max_length=255, blank=False, null=False)
    date_of_birth = DateField(blank=False, null=False)
    room_number = PositiveIntegerField(blank=False, null=False)
    house = ForeignKey('house.House', on_delete=models.CASCADE)
