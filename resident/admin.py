from django.contrib import admin

from resident.models import Resident


class ResidentAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'date_of_birth', 'room_number', 'house')


admin.site.register(Resident, ResidentAdmin)
